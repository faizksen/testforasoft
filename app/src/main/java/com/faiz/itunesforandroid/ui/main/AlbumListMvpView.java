package com.faiz.itunesforandroid.ui.main;

import com.faiz.itunesforandroid.data.model.Album;
import com.faiz.itunesforandroid.ui.base.MvpView;

import java.util.ArrayList;

interface AlbumListMvpView extends MvpView {

    void setListLoading(boolean isLoading);

    void setItems(ArrayList<Album> items);

    void showEmptyResultScreen(boolean isVisible);

    void showRecycler(boolean isVisible);

    void showStartText(boolean isVisible);
}
