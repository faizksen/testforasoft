package com.faiz.itunesforandroid.ui.main;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import com.faiz.itunesforandroid.data.ServiceProvider;
import com.faiz.itunesforandroid.data.model.Album;
import com.faiz.itunesforandroid.ui.album.AlbumFragment;
import com.faiz.itunesforandroid.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.reactivex.disposables.Disposable;

public class AlbumListPresenter extends BasePresenter<AlbumListMvpView> {

    private static final String SAVED_ALBUMS = "saved_albums";
    private static final String SAVED_QUERY = "saved_query";

    private ArrayList<Album> albums;
    private String query;

    @Override
    public void attachView(AlbumListMvpView mvpView, Bundle savedInstanceState) {
        Log.d("TEST1", "AlbumListPresenter attachView");
        super.attachView(mvpView, savedInstanceState);

        if (savedInstanceState != null) {
            albums = (ArrayList<Album>) savedInstanceState.getSerializable(SAVED_ALBUMS);
            query = savedInstanceState.getString(SAVED_QUERY);
            Log.d("TEST1", "AlbumListPresenter attachView albums = " + (albums == null?"NULL": albums.size()));
            Log.d("TEST1", "AlbumListPresenter attachView query = " + query);
        }
        if (albums != null && !albums.isEmpty()) {
            getMvpView().showStartText(false);
            getMvpView().showEmptyResultScreen(false);
            getMvpView().showRecycler(true);
            Log.d("TEST1", "AlbumListPresenter attachView setItems()");
            getMvpView().setItems(albums);
        } else if (!TextUtils.isEmpty(query)) {
            performGetAlbums(query);
        }
    }

    @Override
    public void onSaveInstanceData(Bundle outData) {
        Log.d("TEST1", "AlbumListPresenter onSaveInstanceData");
        outData.putSerializable(SAVED_ALBUMS, albums);
        outData.putString(SAVED_QUERY, query);
        super.onSaveInstanceData(outData);
    }

    public void onSearch(String query) {
        if (!TextUtils.isEmpty(query)) {
            this.query = query;
            performGetAlbums(query);
        }
    }

    private void performGetAlbums(String query) {
        if (getMvpView() == null) {
            return;
        }
        getMvpView().showStartText(false);
        getMvpView().showEmptyResultScreen(false);
        getMvpView().showRecycler(true);
        getMvpView().setListLoading(true);
        Disposable disposable = ServiceProvider.getInstance().getRestService()
                .getAlbumList(query)
                .subscribe(
                        answerAlbumList -> {
                            albums = answerAlbumList.albums;
                            if (albums == null) {
                                albums = new ArrayList<>();
                            }
                            if (albums.isEmpty()) {
                                getMvpView().showRecycler(false);
                                getMvpView().showEmptyResultScreen(true);
                            } else {
                                sortAlbums(albums);
                                getMvpView().setItems(albums);
                            }
                            getMvpView().setListLoading(false);
                        },
                        throwable -> {
                            throwable.printStackTrace();
                            if (getMvpView() != null) {
                                getMvpView().setListLoading(false);
                            }
                        }
                );
        pendingRequestsDisposable.add(disposable);
    }

    private void sortAlbums(ArrayList<Album> albums) {
        Collections.sort(albums, (o1, o2) -> o1.collectionName.compareTo(o2.collectionName));
    }

    public void onAlbumPressed(int position) {
        if (albums != null && albums.size() > position) {
            Album album = albums.get(position);
            if (album != null) {
                showFragment(AlbumFragment.newInstance(album.collectionId));
            }
        }
    }
}
