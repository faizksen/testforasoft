package com.faiz.itunesforandroid.ui.base;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.faiz.itunesforandroid.ui.MainActivity;

public interface MvpView {
    MainActivity getMainActivity();

    void showFragment(Fragment fragment);

    void finish();

    void finish(boolean isSuccess, Intent resultIntent);
}
