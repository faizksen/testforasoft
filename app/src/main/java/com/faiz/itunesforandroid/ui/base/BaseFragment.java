package com.faiz.itunesforandroid.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.faiz.itunesforandroid.data.ServiceProvider;
import com.faiz.itunesforandroid.ui.MainActivity;

public abstract class BaseFragment extends Fragment implements MvpView {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "BaseFragment onCreate");
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            Log.d("TEST1", "BaseFragment onCreate getState();");
            Bundle savedData = ServiceProvider.getInstance().getDatabaseService().getState();
            if (savedData != null) {
                Log.d("TEST1", "BaseFragment onCreate savedData: " + savedData.toString());
                savedInstanceState.putAll(savedData);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "BaseFragment onCreateView");
        View view = inflater.inflate(getLayoutResId(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "BaseFragment onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "BaseFragment onViewStateRestored " + (savedInstanceState==null?"NULL":savedInstanceState.isEmpty()));
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.d("TEST1", "BaseFragment onStart");
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "BaseFragment onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        Log.d("TEST1", "BaseFragment onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.d("TEST1", "BaseFragment onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d("TEST1", "BaseFragment onDestroyView");
        BasePresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.detachView();
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d("TEST1", "BaseFragment onDestroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d("TEST1", "BaseFragment onSaveInstanceState");
        getPresenter().onSaveInstanceState(outState);
        Bundle outData = new Bundle();
        getPresenter().onSaveInstanceData(outData);
        ServiceProvider.getInstance().getDatabaseService().saveState(outData);
        super.onSaveInstanceState(outState);
    }

    protected abstract @LayoutRes
    int getLayoutResId();

    public abstract BasePresenter getPresenter();

    public void showFragment(Fragment fragment) {
        Log.d("TEST1", "BaseFragment showFragment");
        MainActivity activity = getMainActivity();
        if (activity != null) {
            activity.showFragment(fragment, true);
        }
    }

    public MainActivity getMainActivity () {
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            return (MainActivity) activity;
        }
        return null;
    }

    @Override
    public void finish() {
        finish(false, null);
    }

    @Override
    public void finish (boolean isSuccess, Intent resultIntent) {
        MainActivity activity = getMainActivity();
        if (activity != null) {
            Fragment targetFragment = getTargetFragment();
            if (targetFragment instanceof BaseFragment
                    && ((BaseFragment) targetFragment).getPresenter().isViewAttached()
                    && getPresenter().getRequestCode() != 0) {
                targetFragment.onActivityResult(getPresenter().getRequestCode(), isSuccess ? Activity.RESULT_OK : Activity.RESULT_CANCELED, resultIntent);
            }
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager != null) {
                try {
                    fragmentManager.popBackStackImmediate();
                } catch (IllegalStateException e) {
                    fragmentManager.popBackStack();
                    e.printStackTrace();
                }
            }
        }
    }
}
