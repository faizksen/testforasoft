package com.faiz.itunesforandroid.ui.main;

import android.app.SearchManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.faiz.itunesforandroid.R;
import com.faiz.itunesforandroid.data.ServiceProvider;
import com.faiz.itunesforandroid.data.model.Album;
import com.faiz.itunesforandroid.ui.base.BasePresenter;
import com.faiz.itunesforandroid.ui.base.BaseToolbarFragment;
import com.faiz.itunesforandroid.util.ItemClickSupport;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import static android.content.Context.SEARCH_SERVICE;

public class AlbumListFragment extends BaseToolbarFragment implements AlbumListMvpView {

    private AlbumListPresenter presenter = new AlbumListPresenter();

    private ViewGroup emptyScreen;
    private ShimmerRecyclerView recyclerView;
    private AlbumListAdapter adapter;
    private SearchView searchView;
    private TextView startText;

    public static AlbumListFragment newInstance() {
        return new AlbumListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_album_list;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "AlbumListFragment onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        emptyScreen = view.findViewById(R.id.empty_screen);
        showEmptyResultScreen(false);

        startText = view.findViewById(R.id.start_text);
        showStartText(true);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new AlbumListAdapter();
        recyclerView.setAdapter(adapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener((recyclerView1, position, v) -> presenter.onAlbumPressed(position));

        showRecycler(false);
        setTitle(ServiceProvider.getInstance().getApplicationContext().getString(R.string.search));
        presenter.attachView(this, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "AlbumListFragment onViewStateRestored " + (savedInstanceState==null?"NULL":savedInstanceState.isEmpty()));
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.d("TEST1", "AlbumListFragment onStart");
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d("TEST1", "AlbumListFragment onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        Log.d("TEST1", "AlbumListFragment onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.d("TEST1", "AlbumListFragment onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d("TEST1", "AlbumListFragment onDestroy");
        emptyScreen = null;
        recyclerView = null;
        adapter = null;
        searchView = null;
        startText = null;
        super.onDestroy();
    }

    @Override
    protected int setUpMenuItems() {
        return R.menu.search_menu;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem search = menu.findItem(R.id.action_search);
        searchView = (SearchView) search.getActionView();
        SearchManager searchManager = (SearchManager) getMainActivity().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getMainActivity().getComponentName()));
        searchView.setQueryHint(ServiceProvider.getInstance().getApplicationContext().getResources().getString(R.string.search_query_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.onSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void setListLoading(boolean isLoading) {
        if (isLoading) {
            recyclerView.showShimmerAdapter();
        } else {
            recyclerView.hideShimmerAdapter();
        }
    }

    @Override
    public void setItems(ArrayList<Album> items) {
        if (adapter != null) {
            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showEmptyResultScreen(boolean isVisible) {
        emptyScreen.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showRecycler(boolean isVisible) {
        recyclerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showStartText(boolean isVisible) {
        startText.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.AlbumListViewHolder> {

        private ArrayList<Album> items;

        public void setItems(ArrayList<Album> items) {
            this.items = items;
        }

        @NonNull
        @Override
        public AlbumListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new AlbumListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album_list, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull AlbumListViewHolder holder, int position) {
            Album album = items.get(position);
            if (album != null) {
                holder.title.setText(album.collectionName);
                holder.artist.setText(album.artistName);
                Glide.with(holder.imageView.getContext())
                        .load(album.artworkUrl60)
                        .apply(RequestOptions
                                .centerCropTransform()
                                .error(R.drawable.ic_logo)
                                .placeholder(R.drawable.ic_logo))
                        .into(holder.imageView);
            }
        }

        @Override
        public int getItemCount() {
            return items == null ? 0 : items.size();
        }

        public class AlbumListViewHolder extends RecyclerView.ViewHolder {

            public RoundedImageView imageView;
            public TextView title;
            public TextView artist;

            public AlbumListViewHolder(@NonNull View itemView) {
                super(itemView);

                imageView = itemView.findViewById(R.id.image);
                title = itemView.findViewById(R.id.title);
                artist = itemView.findViewById(R.id.artist);
            }
        }
    }
}
