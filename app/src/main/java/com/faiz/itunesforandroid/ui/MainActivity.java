package com.faiz.itunesforandroid.ui;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.faiz.itunesforandroid.R;
import com.faiz.itunesforandroid.ui.main.AlbumListFragment;

public class MainActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("TEST1", "MainActivity onCreate savedInstanceState = " + (savedInstanceState==null?"NULL":savedInstanceState.isEmpty()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            Log.d("TEST1", "MainActivity onCreate showFragment(new AlbumListFragment(), false);");
            showFragment(new AlbumListFragment(), false);
        }
    }

    @Override
    protected void onStart() {
        Log.d("TEST1", "MainActivity onStart");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d("TEST1", "MainActivity onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.d("TEST1", "MainActivity onResume");
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d("TEST1", "MainActivity onRestoreInstanceState savedInstanceState = " + (savedInstanceState==null?"NULL":savedInstanceState.isEmpty()));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStop() {
        Log.d("TEST1", "MainActivity onStop");
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Log.d("TEST1", "MainActivity onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onDestroy() {
        Log.d("TEST1", "MainActivity onDestroy");
        super.onDestroy();
    }

    public void showFragment (Fragment fragment, boolean addToBackStack) {
        Log.d("TEST1", "MainActivity showFragment");
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager == null || fragment == null) {
            return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.main_activity_fragment_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
