package com.faiz.itunesforandroid.ui.base;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.faiz.itunesforandroid.data.ResourceHelper;
import com.faiz.itunesforandroid.data.ServiceProvider;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    protected T mvpView;

    protected ResourceHelper resourceHelper;

    protected CompositeDisposable pendingRequestsDisposable = new CompositeDisposable();
    private int requestCode;

    public BasePresenter() {
        resourceHelper = ServiceProvider.getInstance().getResourceHelper();
    }

    @Override
    public void attachView(T mvpView, Bundle savedInstanceState) {
        Log.d("TEST1", "BasePresenter attachView");
        this.mvpView = mvpView;
    }

    @Override
    public void detachView() {
        Log.d("TEST1", "BasePresenter detachView");
        mvpView = null;
        if (pendingRequestsDisposable != null) {
            pendingRequestsDisposable.clear();
        }
    }

    public T getMvpView() {
        return mvpView;
    }

    public boolean isViewAttached() {
        return mvpView != null;
    }

    public void onSaveInstanceState (Bundle outState) {

    }

    public void onSaveInstanceData(Bundle outData) {

    }

    public void showFragment(Fragment fragment) {
        Log.d("TEST1", "BasePresenter showFragment");
        showFragmentForResult(fragment, 0);
    }

    public void showFragmentForResult (Fragment fragment, int requestCode) {
        if (getMvpView() instanceof BaseFragment) {
            if (requestCode != 0) {
                BaseFragment targetFragment = (BaseFragment) getMvpView();
                fragment.setTargetFragment(targetFragment, requestCode);
                this.requestCode = requestCode;
            }
            getMvpView().showFragment(fragment);
        }
    }

    public int getRequestCode() {
        return requestCode;
    }
}
