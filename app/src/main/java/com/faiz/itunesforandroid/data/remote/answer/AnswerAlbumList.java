package com.faiz.itunesforandroid.data.remote.answer;

import com.faiz.itunesforandroid.data.model.Album;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerAlbumList {

    public int resultCount;

    @JsonProperty("results")
    public ArrayList<Album> albums;
}
