package com.faiz.itunesforandroid.data.remote;

import com.faiz.itunesforandroid.data.remote.answer.AnswerAlbumList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("search")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Observable<AnswerAlbumList> getAlbums(@Query("term") String term,
                                          @Query("country") String country,
                                          @Query("entity") String album,
                                          @Query("attribute") String albumTerm);
}
