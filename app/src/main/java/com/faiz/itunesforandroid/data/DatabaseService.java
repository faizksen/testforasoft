package com.faiz.itunesforandroid.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;

import com.faiz.itunesforandroid.data.model.Album;
import com.faiz.itunesforandroid.data.model.BaseObject;
import com.faiz.itunesforandroid.data.realm.SavedStateBundle;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DatabaseService {

    private static ExecutorService executorService = Executors.newSingleThreadExecutor();

    private Realm realm;
    private Realm savedRealm;

    public DatabaseService (Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("phonebook.realm")
                .build();
        try {
            realm = Realm.getInstance(config);
        } catch (Exception e) {
            e.printStackTrace();
            Realm.deleteRealm(config);
            realm = Realm.getInstance(config);
        }
        initSavedStateRealm();
    }

    private void initSavedStateRealm() {
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("savedState.realm")
                .build();

        try {
            savedRealm = Realm.getInstance(config);
        } catch (Exception e) {
            e.printStackTrace();
            Realm.deleteRealm(config);
            savedRealm = Realm.getInstance(config);
        }
    }

    public void closeRealm() {
        if (realm != null || !realm.isClosed()) {
            realm.close();
        }
        if (savedRealm != null || !savedRealm.isClosed()) {
            savedRealm.close();
        }
    }

    public void startRealm() {
        if (savedRealm == null || savedRealm.isClosed()) {
            initSavedStateRealm();
        }
    }

    public void saveState(Bundle outData) {
        Log.d("TEST1", "DatabaseService saveState");
        try {
            Parcel parcel = Parcel.obtain();
            outData.setClassLoader(ClassLoader.getSystemClassLoader());
            outData.writeToParcel(parcel, 0);
            byte[] bytes = parcel.marshall();
            parcel.recycle();

            SavedStateBundle savedStateBundle = new SavedStateBundle();
            savedStateBundle.setData(bytes);
            Log.d("TEST1", "savedRealmState is " + (savedRealm == null ? "null" : savedRealm.toString()));
            savedRealm.beginTransaction();
            savedRealm.insertOrUpdate(savedStateBundle);
            savedRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Bundle getState() {
        Log.d("TEST1", "DatabaseService getState");
        try {
            SavedStateBundle savedStateBundle = savedRealm.where(SavedStateBundle.class).findFirst();
            if (savedStateBundle == null) {
                Log.d("TEST1", "DatabaseService getState() savedStateBundle == null");
                return null;
            }

            Parcel parcel = Parcel.obtain();
            byte[] data = savedStateBundle.getData();
            parcel.unmarshall(data, 0, data.length);
            parcel.setDataPosition(0); // This is extremely important!
            Bundle bundle = parcel.readBundle(BaseObject.class.getClassLoader());
            parcel.recycle();
            return bundle;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void handleRealmException(Throwable throwable){
        if (realm.isInTransaction()) {
            realm.cancelTransaction();
        }
        throwable.printStackTrace();
    }
}
