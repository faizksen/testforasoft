package com.faiz.itunesforandroid.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Album implements Serializable {

    public String collectionId;
    public String collectionName;
    public String artworkUrl60;

    public String artistId;
    public String artistName;


}
