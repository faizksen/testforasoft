package com.faiz.itunesforandroid.data;

import android.content.Context;
import android.util.Log;

import com.faiz.itunesforandroid.iTunesForAndroidApplication;

public class ServiceProvider {

    private static ServiceProvider instance;

    private Context applicationContext;
    private ResourceHelper resourceHelper;
    private StorageService storageService;
    private DatabaseService databaseService;
    private RestService restService;

    public static void makeInstance(Context context) {
        instance = new ServiceProvider(context);
    }

    public static ServiceProvider getInstance(){
        if (instance == null) {
            throw new NullPointerException("Service provider is not initialized");
        }
        return instance;
    }

    public ServiceProvider(Context context) {
        applicationContext = context;
        resourceHelper = new ResourceHelper(context);
        storageService = new StorageService(context);
        databaseService = new DatabaseService(context);
        restService = new RestService(context);
    }

    public ResourceHelper getResourceHelper() {
        return resourceHelper;
    }

    public StorageService getStorageService() {
        return storageService;
    }

    public DatabaseService getDatabaseService() {
        return databaseService;
    }

    public RestService getRestService() {
        return restService;
    }

    public Context getApplicationContext(){
        if (applicationContext == null) {
            Log.d("TEST1", "ServiceProvider getApplicationContext() applicationContext==null");
            applicationContext = iTunesForAndroidApplication.getInstance().getApplicationContext();
        }
        return applicationContext;
    }
}
