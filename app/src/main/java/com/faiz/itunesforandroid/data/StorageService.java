package com.faiz.itunesforandroid.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class StorageService {

    public static final String PREFERENCES_NAME = "phone_book_pref";

    private SharedPreferences sharedPreferences;

    public StorageService(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Application.MODE_PRIVATE);
    }
}
