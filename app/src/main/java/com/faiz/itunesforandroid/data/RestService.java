package com.faiz.itunesforandroid.data;

import android.content.Context;
import android.os.AsyncTask;

import com.faiz.itunesforandroid.R;
import com.faiz.itunesforandroid.data.remote.ApiInterface;
import com.faiz.itunesforandroid.data.remote.answer.AnswerAlbumList;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestService {

    private static final String COUNTRY = "RU";

    private ApiInterface apiInterface;

    public RestService(Context context) {
        String apiEndpoint = context.getResources().getString(R.string.base_url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiEndpoint)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(initOkHttpClient())
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    private  <T> ObservableTransformer<T, T> applySchedulers() {
        return observable -> observable
                .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                .observeOn(AndroidSchedulers.mainThread());
    }

    private OkHttpClient initOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }

    public Observable<AnswerAlbumList> getAlbumList(String term) {
        String album = ServiceProvider.getInstance().getResourceHelper().getString(R.string.query_entity_album);
        String albumTerm = ServiceProvider.getInstance().getResourceHelper().getString(R.string.query_attribute_album_term);
        return apiInterface
                .getAlbums(term, COUNTRY, album, albumTerm)
                .compose(applySchedulers());
    }
}
