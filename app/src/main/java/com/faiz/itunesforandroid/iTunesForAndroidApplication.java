package com.faiz.itunesforandroid;

import android.app.Application;

import com.faiz.itunesforandroid.data.ServiceProvider;

public class iTunesForAndroidApplication extends Application {

    private static iTunesForAndroidApplication instance;

    public static iTunesForAndroidApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ServiceProvider.makeInstance(this);
    }
}
